#!/bin/bash

cd /go/src/bitbucket.org/Felian/test_task_4
/go/bin/goose --env=docker -path=goose up
cd /app
/app/main --port=8080 --db="host=postgres port=5432 user=postgres password=postgres dbname=test_task_4 sslmode=disable client_encoding=utf8"