package main

import (
	"fmt"
	"log"

	"bitbucket.org/Felian/test_task_4/api"
	"bitbucket.org/Felian/test_task_4/config"
	"bitbucket.org/Felian/test_task_4/service"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func main() {
	// initialize configuration
	if err := config.Parse(); err != nil {
		log.Fatalln(err)
	}

	// initialize service
	service.New()
	s := service.Get()

	// register and run a HTTP server
	RegisterHTTPAPIHandlers(s.HTTPServer)
	if err := s.HTTPServer.Run(fmt.Sprintf(":%d", viper.GetInt(config.Port))); err != nil {
		log.Fatalf("HTTP server error: %v", err)
	}
}

// RegisterHTTPAPIHandlers registers HTTP API handlers
func RegisterHTTPAPIHandlers(router *gin.Engine) {
	router.POST("/take", api.Take)
	router.POST("/fund", api.Fund)
	router.POST("/announceTournament", api.AnnounceTournament)
	router.POST("/joinTournament", api.JoinTournament)
	router.GET("/resultTournament", api.ResultTournament)
	router.GET("/balance", api.Balance)
}
