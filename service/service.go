package service

import (
	"sync"

	"bitbucket.org/Felian/test_task_4/storage"
	"bitbucket.org/Felian/test_task_4/tournaments"
	"github.com/gin-gonic/gin"
)

// Service represents the service internal components
type Service struct {
	sync.Mutex
	Storage     storage.Keeper
	HTTPServer  *gin.Engine
	Tournaments tournaments.TournamentMap
}

var singleton *Service

// Get provides access to a service components
func Get() *Service { return singleton }

// New creates new service object
func New() {
	var keeper storage.Keeper
	keeper = storage.NewPostgres()
	singleton = &Service{
		Storage:     keeper,
		HTTPServer:  gin.Default(),
		Tournaments: tournaments.New(),
	}
}
