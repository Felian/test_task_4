package main_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/Felian/test_task_4/api"
	"bitbucket.org/Felian/test_task_4/config"
	"bitbucket.org/Felian/test_task_4/models"
	"bitbucket.org/Felian/test_task_4/service"
	"bitbucket.org/Felian/test_task_4/storage"
	"bitbucket.org/Felian/test_task_4/tournaments"
	. "github.com/mtfelian/types/decimal"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/spf13/viper"
)

var _ = Describe("Testing with Ginkgo", func() {
	var (
		s       *service.Service
		db      storage.Keeper
		t       tournaments.TournamentMap
		players []models.Player
	)

	BeforeEach(func() {
		s = service.Get()
		s.Storage = storage.NewPostgresWithDB(initDB(viper.GetString(config.DSN)))
		db = s.Storage
		Expect(db.DeleteAll()).To(Succeed())
		t = s.Tournaments

		// create players
		const playersAmount = 6
		players = nil
		for i := 0; i < playersAmount; i++ {
			p, err := db.AddPlayer(Zero)
			Expect(err).NotTo(HaveOccurred())
			Expect(p).NotTo(BeNil())
			players = append(players, *p)
		}
	})

	AfterEach(func() {
		t.DeleteAll()
		db.DeleteAll()
	})

	mustCallAPIFund := func(playerID uint, balance Decimal, expectedStatus int) {
		addr := fmt.Sprintf("%s/fund?playerId=%d&points=%d", server.URL, playerID, balance.I())
		resp := doRequest(http.MethodPost, addr, nil)
		ExpectWithOffset(1, resp.StatusCode).To(Equal(expectedStatus))
	}

	mustCallAPITake := func(playerID uint, balance Decimal, expectedStatus int) {
		addr := fmt.Sprintf("%s/take?playerId=%d&points=%d", server.URL, playerID, balance.I())
		resp := doRequest(http.MethodPost, addr, nil)
		ExpectWithOffset(1, resp.StatusCode).To(Equal(expectedStatus))
	}

	mustCallAnnounceTournament := func(size uint, entryFee Decimal, expectedStatus int) uint {
		addr := fmt.Sprintf("%s/announceTournament?size=%d&deposit=%d", server.URL, size, entryFee.I())
		resp := doRequest(http.MethodPost, addr, nil)
		ExpectWithOffset(1, resp.StatusCode).To(Equal(expectedStatus))

		b, err := ioutil.ReadAll(resp.Body)
		ExpectWithOffset(1, err).NotTo(HaveOccurred())
		defer resp.Body.Close()

		var r api.AnnounceTournamentResponse
		ExpectWithOffset(1, json.Unmarshal(b, &r)).To(Succeed())

		return r.TournamentID
	}

	mustCallJoinTournament := func(tournamentID uint, playerID uint, expectedStatus int, backerID ...uint) {
		addr := fmt.Sprintf("%s/joinTournament?tournamentId=%d&playerId=%d", server.URL,
			tournamentID, playerID)

		aBackerIDs := make([]string, len(backerID))
		for i := range backerID {
			aBackerIDs[i] = strconv.Itoa(int(backerID[i]))
		}
		if len(backerID) > 0 {
			sBackerIDs := strings.Join(aBackerIDs, ",")
			addr += "&backerIds=" + sBackerIDs
		}

		resp := doRequest(http.MethodPost, addr, nil)
		ExpectWithOffset(1, resp.StatusCode).To(Equal(expectedStatus))
	}

	mustCallResultTournament := func(tournamentID uint, expectedStatus int) api.ResultTournamentResponse {
		addr := fmt.Sprintf("%s/resultTournament?tournamentId=%d", server.URL, tournamentID)
		resp := doRequest(http.MethodGet, addr, nil)
		ExpectWithOffset(1, resp.StatusCode).To(Equal(expectedStatus))

		b, err := ioutil.ReadAll(resp.Body)
		ExpectWithOffset(1, err).NotTo(HaveOccurred())
		defer resp.Body.Close()

		var r api.ResultTournamentResponse
		ExpectWithOffset(1, json.Unmarshal(b, &r)).To(Succeed())
		return r
	}

	mustCallBalance := func(playerID uint, expectedStatus int) Decimal {
		addr := fmt.Sprintf("%s/balance?playerId=%d", server.URL, playerID)
		resp := doRequest(http.MethodGet, addr, nil)
		ExpectWithOffset(1, resp.StatusCode).To(Equal(expectedStatus))

		b, err := ioutil.ReadAll(resp.Body)
		ExpectWithOffset(1, err).NotTo(HaveOccurred())
		defer resp.Body.Close()

		var r api.BalanceResponse
		ExpectWithOffset(1, json.Unmarshal(b, &r)).To(Succeed())
		return r.Balance
	}

	entireTournament1 := func() uint {
		mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
		mustCallAPIFund(players[1].ID, I(300), http.StatusOK)
		mustCallAPIFund(players[2].ID, I(300), http.StatusOK)
		mustCallAPIFund(players[3].ID, I(500), http.StatusOK)
		mustCallAPIFund(players[4].ID, I(1000), http.StatusOK)

		const tournamentSize = 5
		tournamentID := mustCallAnnounceTournament(tournamentSize, I(1000), http.StatusCreated)
		tour := t.Get(tournamentID)
		ExpectWithOffset(1, tour.ID).To(Equal(tournamentID))
		ExpectWithOffset(1, tour.Status).To(Equal(models.TournamentCreated))

		mustCallJoinTournament(tournamentID, players[4].ID, http.StatusOK)
		mustCallJoinTournament(tournamentID, players[0].ID, http.StatusOK,
			players[1].ID, players[2].ID, players[3].ID)

		// start, finish the tournament
		tour = t.Get(tournamentID)
		ExpectWithOffset(1, tour.Start()).To(Succeed())
		ExpectWithOffset(1, tour.Complete(players[0].ID)).To(Succeed())
		t.Assign(tour)
		ExpectWithOffset(1, db.WriteRewards(&tour)).To(Succeed())

		return tournamentID
	}

	entireTournament2 := func(winnerIndex int) uint {
		mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
		mustCallAPIFund(players[1].ID, I(300), http.StatusOK)
		mustCallAPIFund(players[2].ID, I(300), http.StatusOK)
		mustCallAPIFund(players[3].ID, I(500), http.StatusOK)
		mustCallAPIFund(players[4].ID, I(1000), http.StatusOK)
		mustCallAPIFund(players[5].ID, I(1000), http.StatusOK)

		const tournamentSize = 6
		tournamentID := mustCallAnnounceTournament(tournamentSize, I(300), http.StatusCreated)
		tour := t.Get(tournamentID)
		ExpectWithOffset(1, tour.ID).To(Equal(tournamentID))
		ExpectWithOffset(1, tour.Status).To(Equal(models.TournamentCreated))

		mustCallJoinTournament(tournamentID, players[0].ID, http.StatusOK)
		mustCallJoinTournament(tournamentID, players[1].ID, http.StatusOK, players[2].ID)
		mustCallJoinTournament(tournamentID, players[3].ID, http.StatusOK, players[4].ID, players[5].ID)

		// start, finish the tournament
		tour = t.Get(tournamentID)
		ExpectWithOffset(1, tour.Start()).To(Succeed())
		ExpectWithOffset(1, tour.Complete(players[winnerIndex].ID)).To(Succeed())
		t.Assign(tour)
		ExpectWithOffset(1, db.WriteRewards(&tour)).To(Succeed())

		return tournamentID
	}

	Context("The entire tournaments", func() {
		Context("entire tournament1", func() {
			It("checks tournament1 from start to finish", func() {
				tournamentID := entireTournament1()

				result := mustCallResultTournament(tournamentID, http.StatusOK)
				Expect(result.Players).To(HaveLen(4)) // winning player + his backers

				// check rewards (including entry fee)
				wd := t.Get(tournamentID).WinnersData.Winners
				Expect(wd[players[0].ID].I()).To(BeEquivalentTo(500))
				Expect(wd[players[1].ID].I()).To(BeEquivalentTo(500))
				Expect(wd[players[2].ID].I()).To(BeEquivalentTo(500))
				Expect(wd[players[3].ID].I()).To(BeEquivalentTo(500))
				Expect(wd[players[4].ID].I()).To(BeEquivalentTo(0))

				Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(I(550))).To(BeTrue())
				Expect(mustCallBalance(players[1].ID, http.StatusOK).Equals(I(550))).To(BeTrue())
				Expect(mustCallBalance(players[2].ID, http.StatusOK).Equals(I(550))).To(BeTrue())
				Expect(mustCallBalance(players[3].ID, http.StatusOK).Equals(I(750))).To(BeTrue())
				Expect(mustCallBalance(players[4].ID, http.StatusOK).Equals(Zero)).To(BeTrue())
			})
		})

		Context("entire tournament2", func() {
			It("if unbacked player0 wins", func() {
				tournamentID := entireTournament2(0)

				result := mustCallResultTournament(tournamentID, http.StatusOK)
				Expect(result.Players).To(HaveLen(1)) // winning player + his backers

				// check rewards (including entry fee)
				wd := t.Get(tournamentID).WinnersData.Winners
				Expect(wd[players[0].ID].I()).To(BeEquivalentTo(900))
				Expect(wd[players[1].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[2].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[3].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[4].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[5].ID].I()).To(BeEquivalentTo(0))

				Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(I(900))).To(BeTrue())
				Expect(mustCallBalance(players[1].ID, http.StatusOK).Equals(I(150))).To(BeTrue())
				Expect(mustCallBalance(players[2].ID, http.StatusOK).Equals(I(150))).To(BeTrue())
				Expect(mustCallBalance(players[3].ID, http.StatusOK).Equals(I(400))).To(BeTrue())
				Expect(mustCallBalance(players[4].ID, http.StatusOK).Equals(I(900))).To(BeTrue())
				Expect(mustCallBalance(players[5].ID, http.StatusOK).Equals(I(900))).To(BeTrue())
			})

			It("if 1-backed player1 wins", func() {
				tournamentID := entireTournament2(1)

				result := mustCallResultTournament(tournamentID, http.StatusOK)
				Expect(result.Players).To(HaveLen(2)) // winning player + his backers

				// check rewards (including entry fee)
				wd := t.Get(tournamentID).WinnersData.Winners
				Expect(wd[players[0].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[1].ID].I()).To(BeEquivalentTo(450))
				Expect(wd[players[2].ID].I()).To(BeEquivalentTo(450))
				Expect(wd[players[3].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[4].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[5].ID].I()).To(BeEquivalentTo(0))

				Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(Zero)).To(BeTrue())
				Expect(mustCallBalance(players[1].ID, http.StatusOK).Equals(I(600))).To(BeTrue())
				Expect(mustCallBalance(players[2].ID, http.StatusOK).Equals(I(600))).To(BeTrue())
				Expect(mustCallBalance(players[3].ID, http.StatusOK).Equals(I(400))).To(BeTrue())
				Expect(mustCallBalance(players[4].ID, http.StatusOK).Equals(I(900))).To(BeTrue())
				Expect(mustCallBalance(players[5].ID, http.StatusOK).Equals(I(900))).To(BeTrue())
			})

			It("if 2-backed player3 wins", func() {
				tournamentID := entireTournament2(3)

				result := mustCallResultTournament(tournamentID, http.StatusOK)
				Expect(result.Players).To(HaveLen(3)) // winning player + his backers

				// check rewards (including entry fee)
				wd := t.Get(tournamentID).WinnersData.Winners
				Expect(wd[players[0].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[1].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[2].ID].I()).To(BeEquivalentTo(0))
				Expect(wd[players[3].ID].I()).To(BeEquivalentTo(300))
				Expect(wd[players[4].ID].I()).To(BeEquivalentTo(300))
				Expect(wd[players[5].ID].I()).To(BeEquivalentTo(300))

				Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(Zero)).To(BeTrue())
				Expect(mustCallBalance(players[1].ID, http.StatusOK).Equals(I(150))).To(BeTrue())
				Expect(mustCallBalance(players[2].ID, http.StatusOK).Equals(I(150))).To(BeTrue())
				Expect(mustCallBalance(players[3].ID, http.StatusOK).Equals(I(700))).To(BeTrue())
				Expect(mustCallBalance(players[4].ID, http.StatusOK).Equals(I(1200))).To(BeTrue())
				Expect(mustCallBalance(players[5].ID, http.StatusOK).Equals(I(1200))).To(BeTrue())
			})
		})

		Context("error", func() {
			It("if somehow backer wins", func() {
				mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
				mustCallAPIFund(players[1].ID, I(300), http.StatusOK)
				mustCallAPIFund(players[2].ID, I(300), http.StatusOK)

				const tournamentSize = 3
				tournamentID := mustCallAnnounceTournament(tournamentSize, I(300), http.StatusCreated)
				tour := t.Get(tournamentID)
				Expect(tour.ID).To(Equal(tournamentID))
				Expect(tour.Status).To(Equal(models.TournamentCreated))

				mustCallJoinTournament(tournamentID, players[0].ID, http.StatusOK)
				mustCallJoinTournament(tournamentID, players[1].ID, http.StatusOK, players[2].ID)

				// start, finish the tournament
				tour = t.Get(tournamentID)
				Expect(tour.Start()).To(Succeed())
				Expect(tour.Complete(players[2].ID)).To(Equal(models.ErrorWinnerIsNotPlayer))
				Expect(tour.Status).To(Equal(models.TournamentRunning))
			})
		})
	})

	Context("Fund / Take / Balance", func() {
		It("is OK, take all", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			mustCallAPITake(players[0].ID, I(300), http.StatusOK)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(Zero)).To(BeTrue())
		})
		It("is OK, take all but 1", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			mustCallAPITake(players[0].ID, I(299), http.StatusOK)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(I(1))).To(BeTrue())
		})
		It("is OK, take more", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			mustCallAPITake(players[0].ID, I(301), http.StatusInternalServerError)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(I(300))).To(BeTrue())
		})

		It("is take with invalid playerId", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			addr := fmt.Sprintf("%s/take?playerId=%s&points=%d", server.URL, "q", 100)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(I(300))).To(BeTrue())
		})

		It("is take with invalid points", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			addr := fmt.Sprintf("%s/take?playerId=%d&points=%s", server.URL, players[0].ID, "q")
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(I(300))).To(BeTrue())
		})

		It("is take with not existent playerId", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			addr := fmt.Sprintf("%s/take?playerId=%d&points=%d", server.URL, 0, 100)
			callAPIForJSONWithSE(http.MethodPost, http.StatusInternalServerError, api.CodeFailedToTake, addr, nil)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(I(300))).To(BeTrue())
		})

		It("is take with broken DB connection", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			Expect(db.Close()).To(Succeed())
			addr := fmt.Sprintf("%s/take?playerId=%d&points=%d", server.URL, players[0].ID, 100)
			callAPIForJSONWithSE(http.MethodPost, http.StatusInternalServerError, api.CodeTransactionFailed, addr, nil)
		})

		It("is fund with invalid playerId", func() {
			addr := fmt.Sprintf("%s/fund?playerId=%s&points=%d", server.URL, "q", 100)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(Zero)).To(BeTrue())
		})

		It("is fund with invalid points", func() {
			addr := fmt.Sprintf("%s/fund?playerId=%d&points=%s", server.URL, players[0].ID, "q")
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(Zero)).To(BeTrue())
		})

		It("is fund with not existent playerId", func() {
			addr := fmt.Sprintf("%s/fund?playerId=%d&points=%d", server.URL, 0, 100)
			callAPIForJSONWithSE(http.MethodPost, http.StatusInternalServerError, api.CodeFailedToFund, addr, nil)
			Expect(mustCallBalance(players[0].ID, http.StatusOK).Equals(Zero)).To(BeTrue())
		})

		It("is fund with broken DB connection", func() {
			Expect(db.Close()).To(Succeed())
			addr := fmt.Sprintf("%s/fund?playerId=%d&points=%d", server.URL, players[0].ID, 100)
			callAPIForJSONWithSE(http.MethodPost, http.StatusInternalServerError, api.CodeFailedToFund, addr, nil)
		})

		It("is balance with invalid playerId", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			addr := fmt.Sprintf("%s/balance?playerId=%s", server.URL, "q")
			callAPIForJSONWithSE(http.MethodGet, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
		})

		It("is balance with not existent playerId", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			addr := fmt.Sprintf("%s/balance?playerId=%d", server.URL, 0)
			callAPIForJSONWithSE(http.MethodGet, http.StatusInternalServerError, api.CodeFailedToGetBalance, addr, nil)
		})

		It("is balance with broken DB connection", func() {
			mustCallAPIFund(players[0].ID, I(300), http.StatusOK)
			Expect(db.Close()).To(Succeed())
			addr := fmt.Sprintf("%s/balance?playerId=%d", server.URL, players[0].ID)
			callAPIForJSONWithSE(http.MethodGet, http.StatusInternalServerError, api.CodeFailedToGetBalance, addr, nil)
		})
	})

	Context("AnnounceTournament with errors", func() {
		It("is with invalid size", func() {
			toursBefore := t.Amount()
			addr := fmt.Sprintf("%s/announceTournament?size=%s&deposit=%d", server.URL, "q", 1000)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
			Expect(t.Amount()).To(Equal(toursBefore))
		})

		It("is with invalid deposit", func() {
			toursBefore := t.Amount()
			addr := fmt.Sprintf("%s/announceTournament?size=%d&deposit=%s", server.URL, 5, "q")
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
			Expect(t.Amount()).To(Equal(toursBefore))
		})

		It("is with too small size", func() {
			toursBefore := t.Amount()
			addr := fmt.Sprintf("%s/announceTournament?size=%d&deposit=%d", server.URL, 1, 1000)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeInvalidTournamentParams, addr, nil)
			Expect(t.Amount()).To(Equal(toursBefore))
		})

		It("is with too small deposit", func() {
			toursBefore := t.Amount()
			addr := fmt.Sprintf("%s/announceTournament?size=%d&deposit=%d", server.URL, 5, 0)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeInvalidTournamentParams, addr, nil)
			Expect(t.Amount()).To(Equal(toursBefore))
		})

		It("is with with broken DB connection", func() {
			toursBefore := t.Amount()
			Expect(db.Close()).To(Succeed())
			addr := fmt.Sprintf("%s/announceTournament?size=%d&deposit=%d", server.URL, 5, 0)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeInvalidTournamentParams, addr, nil)
			Expect(t.Amount()).To(Equal(toursBefore))
		})
	})

	Context("JoinTournament with errors", func() {
		var tournamentID uint
		BeforeEach(func() {
			mustCallAPIFund(players[0].ID, I(1000), http.StatusOK)
			mustCallAPIFund(players[1].ID, I(1000), http.StatusOK)
			mustCallAPIFund(players[2].ID, I(1000), http.StatusOK)
			mustCallAPIFund(players[3].ID, I(1000), http.StatusOK)
			mustCallAPIFund(players[4].ID, I(999), http.StatusOK)

			const tournamentSize = 2
			tournamentID = mustCallAnnounceTournament(tournamentSize, I(1000), http.StatusCreated)
			tour := t.Get(tournamentID)
			Expect(tour.ID).To(Equal(tournamentID))
			Expect(tour.Status).To(Equal(models.TournamentCreated))
		})

		It("is with invalid tournamentId", func() {
			addr := fmt.Sprintf("%s/joinTournament?tournamentId=%s&playerId=%d", server.URL, "w", players[0].ID)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
		})

		It("is with invalid playerId", func() {
			addr := fmt.Sprintf("%s/joinTournament?tournamentId=%d&playerId=%s", server.URL, tournamentID, "w")
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
		})

		It("is if tournament is full", func() {
			mustCallJoinTournament(tournamentID, players[0].ID, http.StatusOK)
			mustCallJoinTournament(tournamentID, players[1].ID, http.StatusOK)
			addr := fmt.Sprintf("%s/joinTournament?tournamentId=%d&playerId=%d", server.URL, tournamentID, players[2].ID)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeTournamentIsFull, addr, nil)
		})

		It("is if not enough for entry fee", func() {
			addr := fmt.Sprintf("%s/joinTournament?tournamentId=%d&playerId=%d", server.URL, tournamentID, players[4].ID)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeFailedToWriteJoinFee, addr, nil)
		})

		It("is if already participating as player", func() {
			mustCallJoinTournament(tournamentID, players[0].ID, http.StatusOK)
			addr := fmt.Sprintf("%s/joinTournament?tournamentId=%d&playerId=%d", server.URL, tournamentID, players[0].ID)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeAlreadyParticipating, addr, nil)
		})

		It("is if already participating as backer", func() {
			mustCallJoinTournament(tournamentID, players[0].ID, http.StatusOK, players[1].ID)
			addr := fmt.Sprintf("%s/joinTournament?tournamentId=%d&playerId=%d", server.URL, tournamentID, players[1].ID)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeAlreadyParticipating, addr, nil)
		})

		It("is with with broken DB connection", func() {
			Expect(db.Close()).To(Succeed())
			addr := fmt.Sprintf("%s/joinTournament?tournamentId=%d&playerId=%d", server.URL, tournamentID, players[0].ID)
			callAPIForJSONWithSE(http.MethodPost, http.StatusUnprocessableEntity, api.CodeFailedToWriteJoinFee, addr, nil)
		})
	})

	Context("ResultTournament with errors", func() {
		var tournamentID uint
		BeforeEach(func() { tournamentID = entireTournament1() })

		It("is with invalid tournamentId", func() {
			addr := fmt.Sprintf("%s/resultTournament?tournamentId=%s", server.URL, "w")
			callAPIForJSONWithSE(http.MethodGet, http.StatusUnprocessableEntity, api.CodeWrongParams, addr, nil)
		})

		It("is with with broken DB connection", func() {
			Expect(db.Close()).To(Succeed())
			addr := fmt.Sprintf("%s/resultTournament?tournamentId=%d", server.URL, tournamentID)
			callAPIForJSONWithSE(http.MethodGet, http.StatusInternalServerError, api.CodeFailedToGetTournamentResults, addr, nil)
		})
	})
})
