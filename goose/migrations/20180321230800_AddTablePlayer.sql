-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE player (
  id              SERIAL PRIMARY KEY,
  created_at      TIMESTAMPTZ NOT NULL DEFAULT (now()),
  updated_at      TIMESTAMPTZ NOT NULL DEFAULT (now()),
  deleted_at      TIMESTAMPTZ NULL DEFAULT NULL,
  balance         DECIMAL(10,4) NOT NULL DEFAULT 0
);


COMMENT ON COLUMN player.created_at IS 'Created';
COMMENT ON COLUMN player.updated_at IS 'Updated';
COMMENT ON COLUMN player.deleted_at IS 'Soft delete';
COMMENT ON COLUMN player.balance IS 'Balance amount';


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE player CASCADE;
