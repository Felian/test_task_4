-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE bill (
  id              SERIAL PRIMARY KEY,
  created_at      TIMESTAMPTZ NOT NULL DEFAULT (now()),
  updated_at      TIMESTAMPTZ NOT NULL DEFAULT (now()),
  deleted_at      TIMESTAMPTZ NULL DEFAULT NULL,
  status          VARCHAR(20) NOT NULL DEFAULT '',
  sum             DECIMAL(10,4) NOT NULL DEFAULT 0,
  player_id       INT NOT NULL,
  tournament_id   INT NULL DEFAULT NULL,
  comment         TEXT
);

ALTER TABLE bill ADD CONSTRAINT bill_player_fk
  FOREIGN KEY (player_id) REFERENCES player (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

COMMENT ON COLUMN bill.created_at IS 'Created';
COMMENT ON COLUMN bill.updated_at IS 'Updated';
COMMENT ON COLUMN bill.deleted_at IS 'Soft delete';
COMMENT ON COLUMN bill.status IS 'Status';
COMMENT ON COLUMN bill.sum IS 'Sum';
COMMENT ON COLUMN bill.player_id IS 'Player ID';
COMMENT ON COLUMN bill.tournament_id IS 'Tournament ID';
COMMENT ON COLUMN bill.comment IS 'Comment';

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE bill CASCADE;
