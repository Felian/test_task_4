-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE tournament (
  id              SERIAL PRIMARY KEY,
  created_at      TIMESTAMPTZ NOT NULL DEFAULT (now()),
  updated_at      TIMESTAMPTZ NOT NULL DEFAULT (now()),
  deleted_at      TIMESTAMPTZ NULL DEFAULT NULL,
  status          VARCHAR(20) NOT NULL DEFAULT '',
  count           INT NOT NULL DEFAULT 0,
  size            INT NOT NULL DEFAULT 0,
  entry_fee       DECIMAL(10,4) NOT NULL DEFAULT 0,
  player_data     JSON NULL DEFAULT NULL,
  winners_data    JSON NULL DEFAULT NULL
);


COMMENT ON COLUMN tournament.created_at IS 'Created';
COMMENT ON COLUMN tournament.updated_at IS 'Updated';
COMMENT ON COLUMN tournament.deleted_at IS 'Soft delete';
COMMENT ON COLUMN tournament.status IS 'Status';
COMMENT ON COLUMN tournament.count IS 'Current players count';
COMMENT ON COLUMN tournament.size IS 'Max players count';
COMMENT ON COLUMN tournament.entry_fee IS 'Entry fee';
COMMENT ON COLUMN tournament.player_data IS 'Players and backers state';
COMMENT ON COLUMN tournament.winners_data IS 'Winners and prizes state';

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE tournament CASCADE;

