package main_test

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"
	"time"

	"bitbucket.org/Felian/test_task_4"
	"bitbucket.org/Felian/test_task_4/config"
	"bitbucket.org/Felian/test_task_4/service"
	"bitbucket.org/Felian/test_task_4/storage"
	"github.com/jinzhu/gorm"
	e "github.com/mtfelian/error"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/spf13/viper"
)

var server *httptest.Server

// callAPIForJSONWithSE calls an API method with hunger for StandardError inside response JSON
func callAPIForJSONWithSE(method string, expectedStatus int, expectedCode uint, address string, body io.Reader) {
	resp := doRequest(method, address, body)
	ExpectWithOffset(1, resp.StatusCode).To(Equal(expectedStatus))
	b, err := ioutil.ReadAll(resp.Body)
	ExpectWithOffset(1, err).NotTo(HaveOccurred())
	defer resp.Body.Close()
	var r e.StandardError
	ExpectWithOffset(1, json.Unmarshal(b, &r)).To(Succeed())
	ExpectWithOffset(1, r.Code()).To(Equal(expectedCode))
}

func doRequest(method, addr string, body io.Reader) *http.Response {
	request, err := http.NewRequest(method, addr, body)
	ExpectWithOffset(2, err).NotTo(HaveOccurred())
	resp, err := http.DefaultClient.Do(request)
	ExpectWithOffset(2, err).NotTo(HaveOccurred())
	return resp
}

// initDB initializes DB for tests with given DSN string
func initDB(DSN string) *gorm.DB {
	db, err := gorm.Open(viper.GetString(config.DBDriver), DSN)
	Expect(err).NotTo(HaveOccurred())
	return db
}

var _ = Describe("register suite hooks", func() {
	BeforeSuite(func() {
		// set DSN
		testsDSN, err := storage.PostgresDSNTests()
		Expect(err).NotTo(HaveOccurred())
		viper.Set(config.DSN, testsDSN)
		viper.Set(config.DBDriver, storage.DriverNamePostgres)

		service.New()
		s := service.Get()
		httpServer := s.HTTPServer
		main.RegisterHTTPAPIHandlers(httpServer)
		server = httptest.NewServer(httpServer)

		// set server port if needed
		URL, err := url.Parse(server.URL)
		Expect(err).NotTo(HaveOccurred())
		port, err := strconv.Atoi(URL.Port())
		Expect(err).NotTo(HaveOccurred())
		viper.Set(config.Port, uint(port))

		Expect(s.Storage).NotTo(BeNil())
		Expect(s.Storage.DeleteAll()).To(Succeed())
	})

	AfterSuite(func() { server.Close() })
})

func TestAll(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	RegisterFailHandler(Fail)
	RunSpecs(t, "Main Suite")
}
