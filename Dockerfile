FROM golang

ENV GOBIN /go/bin

RUN mkdir /app
RUN mkdir -p /go/src/bitbucket.org/Felian
WORKDIR /go/src/bitbucket.org/Felian
RUN git clone https://bitbucket.org/Felian/test_task_4
WORKDIR /go/src/bitbucket.org/Felian/test_task_4

RUN go get -u github.com/golang/dep/...
RUN dep ensure -vendor-only
RUN go get -u bitbucket.org/liamstask/goose/cmd/goose

# Build my app
RUN go build -o /app/main .
EXPOSE 8080
ENTRYPOINT ["/bin/bash", "-c", "/go/src/bitbucket.org/Felian/test_task_4/docker.sh"]
