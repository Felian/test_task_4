package storage

import (
	"bitbucket.org/Felian/test_task_4/models"
	"github.com/jinzhu/gorm"
	. "github.com/mtfelian/types/decimal"
)

// Keeper provides access log storage abstraction
type Keeper interface {
	AddTournament(deposit Decimal, size uint) (*models.Tournament, error)
	AddPlayer(balance Decimal) (*models.Player, error)

	Begin() (*gorm.DB, error)
	End(tx *gorm.DB, err error) error

	PlayerGetBalance(tx *gorm.DB, ID uint) (Decimal, error)
	BillsByTournamentID(tournamentID uint) ([]models.Bill, error)

	Fund(playerID uint, sum Decimal) error
	Take(playerID uint, sum Decimal) error

	WriteJoinFee(participantIDs []uint, tournamentID uint, withdrawSum Decimal) error
	WriteRewards(tournament *models.Tournament) error

	DeleteAll() error
	Close() error
}
