package storage_test

import (
	"bitbucket.org/Felian/test_task_4/config"
	"bitbucket.org/Felian/test_task_4/storage"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/spf13/viper"
)

// initDB initializes DB for tests with given DSN string
func initDB(DSN string) *gorm.DB {
	db, err := gorm.Open(viper.GetString(config.DBDriver), DSN)
	Expect(err).NotTo(HaveOccurred())
	return db
}

var _ = Describe("testing Model1 Model via Postgres storage", func() {
	var keeper storage.Keeper
	BeforeEach(func() {
		testsDSN, err := storage.PostgresDSNTests()
		Expect(err).NotTo(HaveOccurred())
		viper.Set(config.DSN, testsDSN)
		viper.Set(config.DBDriver, storage.DriverNamePostgres)
		keeper = storage.NewPostgresWithDB(initDB(viper.GetString(config.DSN)))
		Expect(keeper.DeleteAll()).To(Succeed())
	})
	AfterEach(func() { Expect(keeper.(storage.Postgres).DB().Close()).To(Succeed()) })

	It("todo", func() {
		// can test something at storage level here
	})
})
