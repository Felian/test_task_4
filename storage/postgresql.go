package storage

import (
	"errors"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"bitbucket.org/Felian/test_task_4/config"
	"bitbucket.org/Felian/test_task_4/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	. "github.com/mtfelian/types/decimal"
	"github.com/mtfelian/utils"
	"github.com/spf13/viper"
)

const (
	BillCommentFund  = "fund"
	BillCommentTake  = "take"
	BillCommentFee   = "fee"
	BillCommentPrize = "prize"
)

// DriverNamePostgres holds driver name for PostgreSQL
const DriverNamePostgres = "postgres"

var ErrorNotEnoughBalance = errors.New("not enough balance")

// PostgresDSNTests returns a postgresql DSN (DB access config string) for testing purposes
func PostgresDSNTests() (string, error) {
	_, path, _, ok := runtime.Caller(0)
	if !ok {
		return "", fmt.Errorf("failed to get Caller(0)")
	}
	const dsnFileName = "tests_dsn.cfg"
	dsnFilePath := filepath.Join(filepath.Dir(path), "..", dsnFileName)
	if utils.FileExists(dsnFilePath) {
		data, err := ioutil.ReadFile(dsnFilePath)
		if err != nil {
			return "", err
		}
		return strings.TrimSpace(string(data)), nil
	}
	return `host=localhost port=5432 user=postgres dbname=test_task_4 sslmode=disable client_encoding=utf8`, nil
}

// DB provides to DB access
func (s Postgres) DB() *gorm.DB {
	if s.db != nil {
		return s.db
	}

	fmt.Println("connecting to DB:", viper.GetString(config.DSN))
	newDB, err := gorm.Open(viper.GetString(config.DBDriver), viper.GetString(config.DSN))
	if err != nil {
		panic(err)
	}
	s.db = newDB

	//s.db.LogMode(true)
	s.db.SingularTable(true)
	return s.db
}

// NewPostgresWithDB works just like NewPostgres but with *gorm.DB pointer specified
func NewPostgresWithDB(db *gorm.DB) Postgres { return Postgres{db: db} }

// NewPostgres creates a new PostgreSQL-based storage via GORM and default/loaded DB settings
func NewPostgres() Postgres {
	p := Postgres{}
	p.db = p.DB()
	return p
}

// Postgres implements Keeper for postgres
type Postgres struct {
	db *gorm.DB
	sync.Mutex
}

// AddTournament creates new tournament
func (s Postgres) AddTournament(deposit Decimal, size uint) (*models.Tournament, error) {
	newTournament := &models.Tournament{
		EntryFee:    deposit,
		Size:        size,
		PlayerData:  models.NewPlayerData(),
		WinnersData: models.NewWinnersData(),
	}
	err := s.DB().Save(newTournament).Error
	return newTournament, err
}

// AddPlayer creates new player
func (s Postgres) AddPlayer(balance Decimal) (*models.Player, error) {
	newPlayer := &models.Player{Balance: balance}
	err := s.DB().Save(newPlayer).Error
	return newPlayer, err
}

// PlayerGetBalance returns balance of a player with given ID
func (s Postgres) PlayerGetBalance(tx *gorm.DB, ID uint) (Decimal, error) {
	if tx == nil {
		tx = s.DB()
	}
	var player models.Player
	err := tx.First(&player, ID).Error
	return player.Balance, err
}

// playerSetBalance with given player ID and sum
func (s Postgres) playerSetBalance(tx *gorm.DB, ID uint, sum Decimal) error {
	if tx == nil {
		tx = s.DB()
	}
	return tx.Model(&models.Player{}).Where("id = ?", ID).Update("balance", sum).Error
}

// Begin the transaction
func (s Postgres) Begin() (*gorm.DB, error) {
	tx := s.DB().Begin()
	return tx, tx.Error
}

// End the transaction in a way according to the err value (nil means Commit, otherwise Rollback)
func (s Postgres) End(tx *gorm.DB, err error) error {
	if tx == nil {
		return nil
	}
	if err != nil {
		return tx.Rollback().Error
	}
	return tx.Commit().Error
}

// addBill for playerID
func (s Postgres) addBill(tx *gorm.DB, playerID uint, tournamentID *uint, sum Decimal, comment string) error {
	if tx == nil {
		tx = s.DB()
	}
	bill := &models.Bill{
		Sum:          sum,
		Comment:      comment,
		TournamentID: tournamentID,
		PlayerID:     playerID,
	}
	return tx.Save(bill).Error
}

// TournamentUpdate
func (s Postgres) TournamentUpdate(tx *gorm.DB, tournament *models.Tournament) error {
	if tx == nil {
		tx = s.DB()
	}
	return tx.Save(tournament).Error
}

// BillsByTournamentID retrieves bills for the given tournamentID
func (s Postgres) BillsByTournamentID(tournamentID uint) ([]models.Bill, error) {
	bills := []models.Bill{}
	err := s.DB().Where("tournament_id = ?", tournamentID).Find(&bills).Error
	return bills, err
}

// Fund the sum from playerID
func (s Postgres) Fund(playerID uint, sum Decimal) error {
	tx, err := s.Begin()
	if err != nil {
		return err
	}
	defer func() { s.End(tx, err) }()

	var balance Decimal
	balance, err = s.PlayerGetBalance(tx, uint(playerID))
	if err != nil {
		return err
	}

	if err = s.addBill(tx, uint(playerID), nil, sum, BillCommentFund); err != nil {
		return err
	}

	if err = s.playerSetBalance(tx, uint(playerID), balance.Add(sum)); err != nil {
		return err
	}
	return nil
}

// Take the sum from playerID
func (s Postgres) Take(playerID uint, sum Decimal) error {
	tx, err := s.Begin()
	if err != nil {
		return err
	}
	defer func() { s.End(tx, err) }()

	var balance Decimal
	balance, err = s.PlayerGetBalance(tx, uint(playerID))
	if err != nil {
		return err
	}

	if balance.Lt(sum) {
		return ErrorNotEnoughBalance
	}

	if err = s.addBill(tx, uint(playerID), nil, sum.Neg(), BillCommentTake); err != nil {
		return err
	}

	if err = s.playerSetBalance(tx, uint(playerID), balance.Sub(sum)); err != nil {
		return err
	}
	return nil
}

// WriteJoinFee for tournament
func (s Postgres) WriteJoinFee(participantIDs []uint, tournamentID uint, withdrawSum Decimal) error {
	tx, err := s.Begin()
	if err != nil {
		return err
	}
	defer func() { s.End(tx, err) }()

	for _, id := range participantIDs {
		err = s.writeFee(tx, id, withdrawSum, tournamentID)
		if err != nil {
			return err
		}
	}
	return nil
}

// writeFee at joining to the tournament
func (s Postgres) writeFee(tx *gorm.DB, playerID uint, withdrawSum Decimal, tournamentID uint) error {
	if tx == nil {
		tx = s.DB()
	}
	var balance Decimal
	var err error
	balance, err = s.PlayerGetBalance(tx, playerID)
	if err != nil {
		return err
	}

	if balance.Lt(withdrawSum) {
		return ErrorNotEnoughBalance
	}

	if err := s.playerSetBalance(tx, playerID, balance.Sub(withdrawSum)); err != nil {
		return err
	}

	return s.addBill(tx, playerID, &tournamentID, withdrawSum.Neg(), BillCommentFee)
}

// WriteRewards about a completed tournament
func (s Postgres) WriteRewards(tournament *models.Tournament) (err error) {
	var tx *gorm.DB
	tx, err = s.Begin()
	if err != nil {
		return
	}
	defer func() { s.End(tx, err) }()

	for id, prize := range tournament.WinnersData.Winners {
		if err = s.addBill(tx, id, &tournament.ID, prize, BillCommentPrize); err != nil {
			return
		}
		var balance Decimal
		balance, err = s.PlayerGetBalance(tx, id)
		if err != nil {
			return
		}
		if err = s.playerSetBalance(tx, id, balance.Add(prize)); err != nil {
			return
		}
	}

	err = tx.Save(tournament).Error
	return
}

// DeleteAll deletes all rows
func (s Postgres) DeleteAll() error {
	if err := s.DB().Unscoped().Delete(models.Bill{}).Error; err != nil {
		return err
	}

	if err := s.DB().Unscoped().Delete(models.Tournament{}).Error; err != nil {
		return err
	}

	return s.DB().Unscoped().Delete(models.Player{}).Error
}

// Close the storage connection
func (s Postgres) Close() error { return s.DB().Close() }
