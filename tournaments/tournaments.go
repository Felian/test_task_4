package tournaments

import (
	"sync"

	"bitbucket.org/Felian/test_task_4/models"
)

// TournamentMap represents tournaments "cache"
type TournamentMap struct {
	sync.Mutex
	m map[uint]models.Tournament
}

// New returns new tournaments
func New() TournamentMap {
	return TournamentMap{m: make(map[uint]models.Tournament)}
}

// Assign a tournament
func (t TournamentMap) Assign(tournament models.Tournament) {
	if tournament.PlayerData.Backers == nil {
		tournament.PlayerData = models.NewPlayerData()
	}
	tournament.Status = models.TournamentCreated
	t.Lock()
	t.m[tournament.ID] = tournament
	t.Unlock()
}

// Remove a tournament
func (t TournamentMap) Remove(ID uint) {
	t.Lock()
	delete(t.m, ID)
	t.Unlock()
}

// Get a tournament
func (t TournamentMap) Get(ID uint) models.Tournament {
	t.Lock()
	defer t.Unlock()
	return t.m[ID]
}

// DeleteAll tournaments
func (t TournamentMap) DeleteAll() {
	for key := range t.m {
		t.Remove(key)
	}
}

// Amount of tournaments
func (t TournamentMap) Amount() int {
	t.Lock()
	defer t.Unlock()
	return len(t.m)
}
