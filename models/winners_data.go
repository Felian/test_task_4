package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"

	. "github.com/mtfelian/types/decimal"
)

// NewWinnersData returns new winners data
func NewWinnersData() WinnersData { return WinnersData{Winners: make(map[uint]Decimal)} }

// WinnersData is a tournament winners data
type WinnersData struct {
	Winners map[uint]Decimal // maps player/backer ID to won sum (including entry fee)
}

// Value makes PlayerData to implement driver.Valuer
func (wd WinnersData) Value() (driver.Value, error) { return json.Marshal(wd) }

// Scan makes PlayerData to implement sql.Scanner
func (wd *WinnersData) Scan(src interface{}) error {
	if bytes, ok := src.([]byte); ok {
		return json.Unmarshal(bytes, wd)

	}
	return errors.New(fmt.Sprint("Failed to unmarshal JSON from DB:", src))
}
