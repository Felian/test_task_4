package models

import (
	"time"

	. "github.com/mtfelian/types/decimal"
)

// Bill model
type Bill struct {
	ID           uint       `gorm:"column:id;primary_key"`
	CreatedAt    time.Time  `gorm:"column:created_at"`
	UpdatedAt    time.Time  `gorm:"column:updated_at"`
	DeletedAt    *time.Time `gorm:"column:deleted_at"`
	Sum          Decimal    `gorm:"column:sum"`
	Comment      string     `gorm:"column:comment"`
	PlayerID     uint       `gorm:"column:player_id"`
	TournamentID *uint      `gorm:"column:tournament_id"`
}

// TableName returns a table name for a model
func (*Bill) TableName() string { return "bill" }
