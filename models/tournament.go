package models

import (
	"errors"
	"time"

	. "github.com/mtfelian/types/decimal"
)

var (
	ErrorWinnerIsNotPlayer    = errors.New("winner is not a player")
	ErrorWrongTournamentState = errors.New("wrong tournament state")
	ErrorNotEnoughPlayers     = errors.New("not enought players")
)

// Tournament model
type Tournament struct {
	ID          uint        `gorm:"column:id;primary_key"`
	CreatedAt   time.Time   `gorm:"column:created_at"`
	UpdatedAt   time.Time   `gorm:"column:updated_at"`
	DeletedAt   *time.Time  `gorm:"column:deleted_at"`
	Status      string      `gorm:"column:status"`
	Count       uint        `gorm:"column:count"`
	Size        uint        `gorm:"column:size"`
	EntryFee    Decimal     `gorm:"column:entry_fee"`
	PlayerData  PlayerData  `gorm:"column:player_data"`
	WinnersData WinnersData `gorm:"column:winners_data"`
}

// TableName returns a table name for a model
func (*Tournament) TableName() string { return "tournament" }

const (
	TournamentCreated   = "created"
	TournamentRunning   = "running"
	TournamentCompleted = "completed"
)

// IsCompleted returns true if tournament is completed
func (t *Tournament) IsComleted() bool { return t.Status == TournamentCompleted }

// Start the tournament
func (t *Tournament) Start() error {
	if t.Status != TournamentCreated {
		return ErrorWrongTournamentState
	}
	if t.Count < 2 {
		return ErrorNotEnoughPlayers
	}
	t.Status = TournamentRunning
	return nil
}

// Complete the tournament with given winner player ID
func (t *Tournament) Complete(winnerID uint) error {
	pd := t.PlayerData
	m := pd.Backers

	if t.Status != TournamentRunning {
		return ErrorWrongTournamentState
	}

	_, exists := m[winnerID]
	if !exists {
		return ErrorWinnerIsNotPlayer
	}

	participantIDs := pd.ParticipantIDs()
	divider := len(m[winnerID]) + 1 // winner's backers count

	fullReward := t.EntryFee.MulI(int(t.Count)).DivI(divider)

	for _, id := range participantIDs {
		if pd.WasBacking(id, winnerID) || id == winnerID {
			t.WinnersData.Winners[id] = fullReward
			continue
		}
		t.WinnersData.Winners[id] = Zero
	}

	t.Status = TournamentCompleted
	return nil
}
