package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/mtfelian/utils"
)

// NewPlayerData returns new player data
func NewPlayerData() PlayerData { return PlayerData{Backers: make(map[uint][]uint)} }

// PlayerData is a tournament players data
type PlayerData struct {
	Backers map[uint][]uint // map playerID to a slice of backerIDs
}

// Value makes PlayerData to implement driver.Valuer
func (pd PlayerData) Value() (driver.Value, error) { return json.Marshal(pd) }

// Scan makes PlayerData to implement sql.Scanner
func (pd *PlayerData) Scan(src interface{}) error {
	if bytes, ok := src.([]byte); ok {
		return json.Unmarshal(bytes, pd)

	}
	return errors.New(fmt.Sprint("Failed to unmarshal JSON from DB:", src))
}

// ParticipantIDs
func (pd *PlayerData) ParticipantIDs() []uint {
	var ids []uint
	for playerID, backerIDs := range pd.Backers {
		ids = append(ids, playerID)
		ids = append(ids, backerIDs...)
	}
	return ids
}

// WasBacking returns true if player with whoID was backing a player with whomID
func (pd *PlayerData) WasBacking(whoID, whomID uint) bool {
	backerIDs, exists := pd.Backers[whomID]
	if !exists {
		return false
	}
	return utils.SliceContains(whoID, backerIDs)
}

// CheckPlayers returns true if player with given ID already participates as a player or a backer
func (pd *PlayerData) CheckPlayer(ID uint) bool {
	for playerID, backerIDs := range pd.Backers {
		if playerID == ID || utils.SliceContains(ID, backerIDs) {
			return true
		}
	}
	return false
}
