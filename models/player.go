package models

import (
	"time"

	. "github.com/mtfelian/types/decimal"
)

// Player model
type Player struct {
	ID        uint       `gorm:"column:id;primary_key"`
	CreatedAt time.Time  `gorm:"column:created_at"`
	UpdatedAt time.Time  `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at"`
	Balance   Decimal    `gorm:"column:balance"`
}

// TableName returns a table name for a model
func (*Player) TableName() string { return "player" }
