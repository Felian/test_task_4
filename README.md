# Test task for Aleksei Makagonov

From HR Aleksei Makagonov for one company Estonia, Tallinn.

## Deploying the app

- First of all, you should install and configure Go and PostgreSQL.
- Clone this repo.
- After installing, create the database, from `psql` console:

```
CREATE DATABASE "test_task_4"
```

- Edit DSN in `goose/dbconf.yml` according to your DB auth configuration:

- Next install the migration tool and apply the migrations:

```
go get bitbucket.org/liamstask/goose/cmd/goose
goose -path=goose up
```

### Coverage analysis

Download and install a tool for cross-package coverage analysis:
```
go get github.com/go-playground/overalls
```

Run the tests and generate a coverprofiles
```
go test -p=1 ./... && overalls -project=bitbucket.org/Felian/test_task_4 -covermode=count -debug -- -coverpkg=./... -p=1
```

You can view the coverprofile in HTML format:
```
go tool cover --html=overalls.coverprofile
```

### Testing

- Ensure that DSN inside func `PostgresDSNTests()` in
`./storage/postgresql.go` returns the right value.
- Launch `go test -p=1 --race ./...` or `ginkgo -r --race` (if you installed Ginkgo)

#### Specifying the custom DSN for tests

In the root project dir create filenamed `tests_dsn.cfg` and write
here DSN as a string.

### Running

```
go build && ./test_task_4 --port=3000 --db='host=localhost port=5432 user=postgres dbname=test_task_4 sslmode=disable client_encoding=utf8'
```

#### Params:

- `port` to specify HTTP server port
- `db` to configure the DSN
- `config` configuration file name (extension will be added automatically)
- `dbdriver` by default `postgres`
- `debug` flag is not used here

### API

Standard error:
```
{
  "code": 1,
  "error": "text"
}
```

Codes:
```
	1  CodeWrongParams
	2  CodeTransactionFailed
	3  CodeFailedToGetBalance
	4  CodeInvalidTournamentParams
	5  CodeFailedToAddTournament
	6  CodeAlreadyParticipating
	7  CodeTournamentIsFull
	8  CodeFailedToWriteJoinFee
	9  CodeFailedToTake
    10 CodeFailedToFund
	11 CodeFailedToGetTournamentResults
```

#### POST /take

Withdraw some money from player.

Query parameters:
```
playerId    uint        ID of a player
points      Decimal     money amount
```

##### Response HTTP:
- 200 - OK
- 422 - wrong input parameters
- 500 - all other

###### OK:
Standard error with code 0.

###### Error:
Standard error.

#### POST /fund

Give some money to player.

Query parameters:
```
playerId    uint        ID of a player
points      Decimal     money amount
```

##### Response HTTP:
- 200 - OK
- 422 - wrong input parameters
- 500 - all other

###### OK:
Standard error with code 0.

###### Error:
Standard error.

#### POST /announceTournament

Create new tournament.

Query parameters:
```
deposit     Decimal     entry fee to play, >= 1
size        uint        max players to join (as players), >= 2
```

##### Response HTTP:
- 201 - OK
- 422 - wrong input parameters
- 500 - all other

###### OK:
```
{
    "code": 0,
    "tournamentId": 1
}
```

###### Error:
Standard error.

#### POST /joinTournament

Join the existing tournament.

Query parameters:
```
tournamentId uint       ID of a tournament
playerId     uint       ID of a joining player
backerIds    string     optional ','-separated ID of a backing players
```

##### Response HTTP:
- 200 - OK
- 422 - wrong input parameters, or tournament is full
- 500 - all other

###### OK:
Standard error with code 0.

###### Error:
Standard error.

#### GET /resultTournament

Get the results of a tournament.

Query parameters:
```
tournamentId uint       ID of a tournament
```

##### Response HTTP:
- 200 - OK
- 422 - wrong input parameters, or tournament is full
- 500 - all other

###### OK:
Contains winning player and his backers (if playd backed).
```
{
    "code": 0,
    "winners": [
        {
            "playerId": 7,
            "prize": 1000
        },
        {
            "playerId": 9,
            "prize": 1000
        }
    ]
}
```

###### Error:
Standard error.

#### GET /balance

Get the player's balance.

Query parameters:
```
playerId     uint       ID of a player
```

##### Response HTTP:
- 200 - OK
- 422 - wrong input parameters, or tournament is full
- 500 - all other

###### OK:
```
{
    "code": 0,
    "playerId": 7,
    "balance": 1000
}
```

###### Error:
Standard error.

## Source

```
Backend developer coding task "Social tournament service"

As a gaming website we want to implement a tournament service with a feature called "Back a friend".
Each player holds certain amount of bonus points. Website funds its players with bonus points based on all kind of activity. Bonus points can
traded to goods and represent value like real money.

One of the social products class is a social tournament. This is a competition between players in a multi-player game like poker, bingo, etc)

Entering a tournament requires a player to deposit certain amount of entry fee in bonus points. If a player has not enough point he can ask other
players to back him and get a part the prize in case of a win.

In case of multiple backers, they submit equal part of the deposit and share the winning money in the same ration.

From a technical side, the following REST service with 5 endpoints should be implemented

#1 Take and fund player account
/take?playerId=P1&points=300   takes 300 points from player P1 account
/fund?playerId=P2&points=300   funds player P2 with 300 points

#2 Announce tournament specifying the entry deposit
/announceTournament?tournamentId=1&deposit=1000

#3 Join player into a tournament and is he backed by a set of backers
/joinTournament?tournamentId=1&playerId=P1&backerId=P2&backerId=P3
Backing is not mandatory and a player can be play on his own money

#4 Result tournament winners and prizes
/resultTournament with a POST document in format
{"winners": [{"playerId": "P1", "prize": 500}]}

#5 Player balance
/balance?playerId=P1
Example response: {"playerId": "P1", "balance": 456.00}

Full use case example:

Prepare initial balances
/fund?playerId=P1&points=300
/fund?playerId=P2&points=300
/fund?playerId=P3&points=300
/fund?playerId=P4&points=500
/fund?playerId=P5&points=1000

Tournament deposit is 1000 points
/announceTournament?tournamentId=1&deposit=1000

P5 joins on his own
/joinTournament?tournamentId=1&playerId=P5

P1 joins backed by P2, P3, P4
/joinTournament?tournamentId=1&playerId=P1&backerId=P2&backerId=P3&backerId=P4

All of them P1, P2, P3, P4 contribute 250 points each.
P1 wins the tournament and his prize is 2000. P2 P3 P4 thet all get 25% of the prize.

/resultTournament with a POST
{"winners": [{"playerId": "P1", "prize": 2000}]}

After tournament result is processed the balances for players must be as specified below
P1, P2, P3 - 550
P4 - 750
P5 - 0

Endpoints 1-4 must return HTTP status codes only like 2xx, 4xx, 5xx
Endpoints 5 must return json document in the format on the example above

Your solution must be delivered as a source code on github / bitbucket and a docker compose script (use 1 free repository at hub.docker.com to
deploy container image)

You can use whatever programming language and any open source data storage.
Feel free to ask for details.
```
