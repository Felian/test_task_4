package api

import (
	"errors"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/Felian/test_task_4/service"
	"github.com/gin-gonic/gin"
	e "github.com/mtfelian/error"
	. "github.com/mtfelian/types/decimal"
	"github.com/mtfelian/utils"
)

const OK = "OK"

const (
	CodeOK uint = iota
	CodeWrongParams
	CodeTransactionFailed
	CodeFailedToGetBalance
	CodeInvalidTournamentParams
	CodeFailedToAddTournament
	CodeAlreadyParticipating
	CodeTournamentIsFull
	CodeFailedToWriteJoinFee
	CodeFailedToTake
	CodeFailedToFund
	CodeFailedToGetTournamentResults
)

// Success contains success "standard error" structure
var Success = e.NewErrorf(CodeOK, OK).(e.StandardError)

var (
	errorFailedToCreate          = errors.New("failed to create entity")
	errorInvalidTournamentParams = errors.New("invalid tournament params")
	errorTournamentIsFull        = errors.New("tournament is full")
	errorAlready                 = errors.New("already")
)

// Take withdraws some amount of money from player
func Take(c *gin.Context) {
	playerID, err := strconv.Atoi(c.Query("playerId"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}
	points, err := NewFromString(c.Query("points"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}

	s := service.Get().Storage
	tx, err := s.Begin()
	if err != nil {
		c.JSON(http.StatusInternalServerError, e.NewError(CodeTransactionFailed, err))
		return
	}
	defer func() { s.End(tx, err) }()

	if err := s.Take(uint(playerID), points); err != nil {
		c.JSON(http.StatusInternalServerError, e.NewError(CodeFailedToTake, err))
		return
	}

	c.JSON(http.StatusOK, Success)
}

// Func gives player some amount of money
func Fund(c *gin.Context) {
	playerID, err := strconv.Atoi(c.Query("playerId"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}
	points, err := NewFromString(c.Query("points"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}

	s := service.Get().Storage
	if err := s.Fund(uint(playerID), points); err != nil {
		c.JSON(http.StatusInternalServerError, e.NewError(CodeFailedToFund, err))
		return
	}

	c.JSON(http.StatusOK, Success)
}

// AnnounceTournamentResponse represents a response for method AnnounceTournament
type AnnounceTournamentResponse struct {
	e.StandardError
	TournamentID uint `json:"tournamentId"`
}

// AnnounceTournament method to start a tournament
func AnnounceTournament(c *gin.Context) {
	deposit, err := NewFromString(c.Query("deposit"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}
	size, err := strconv.Atoi(c.Query("size"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}

	if deposit.Lt(I(1)) || size < 2 {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeInvalidTournamentParams, errorInvalidTournamentParams))
		return
	}

	s := service.Get()
	storage, tours := s.Storage, s.Tournaments
	newTournament, err := storage.AddTournament(deposit, uint(size))
	if err == nil && newTournament == nil {
		err = errorFailedToCreate
	}
	if err != nil {
		c.JSON(http.StatusInternalServerError, e.NewError(CodeFailedToAddTournament, err))
		return
	}
	tours.Assign(*newTournament)

	c.JSON(http.StatusCreated, AnnounceTournamentResponse{
		StandardError: Success,
		TournamentID:  newTournament.ID,
	})
}

// JoinTournament method to join the tournament
func JoinTournament(c *gin.Context) {
	s := service.Get()
	s.Lock()
	defer s.Unlock()

	tournamentID, err := strconv.Atoi(c.Query("tournamentId"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}
	playerID, err := strconv.Atoi(c.Query("playerId"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}
	aBackerIDs := strings.Split(c.Query("backerIds"), ",")
	backerIDs := []uint{}
	for _, aBackerID := range aBackerIDs {
		backerID, err := strconv.Atoi(aBackerID)
		if err != nil {
			continue
		}
		if utils.SliceContains(uint(backerID), backerIDs) {
			continue
		}
		backerIDs = append(backerIDs, uint(backerID))
	}

	storage, tours := s.Storage, s.Tournaments
	tournament := tours.Get(uint(tournamentID))

	if tournament.Count >= tournament.Size {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeTournamentIsFull, errorTournamentIsFull))
		return
	}

	participantsID := append(backerIDs, uint(playerID))
	// check whether player is already participates as player or as a backer
	for _, id := range participantsID {
		if already := tournament.PlayerData.CheckPlayer(id); already {
			c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeAlreadyParticipating, errorAlready))
			return
		}
	}

	withdrawSum := tournament.EntryFee.DivI(len(backerIDs) + 1)

	if err := storage.WriteJoinFee(participantsID, tournament.ID, withdrawSum); err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeFailedToWriteJoinFee, err))
		return
	}

	tournament.PlayerData.Backers[uint(playerID)] = backerIDs
	tournament.Count++
	tours.Assign(tournament)

	c.JSON(http.StatusOK, Success)
}

// ResultTournamentResponsePlayer is a player element for type ResultTournamentResponse
type ResultTournamentResponsePlayer struct {
	PlayerID uint    `json:"playerId"`
	Prize    Decimal `json:"prize"`
}

// ResultTournamentResponse represents a response for method ResultTournament
type ResultTournamentResponse struct {
	e.StandardError
	Players []ResultTournamentResponsePlayer `json:"winners"`
}

// ResultTournament method to get tournament result
func ResultTournament(c *gin.Context) {
	tournamentID, err := strconv.Atoi(c.Query("tournamentId"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}

	s := service.Get().Storage
	bills, err := s.BillsByTournamentID(uint(tournamentID))
	if err != nil {
		c.JSON(http.StatusInternalServerError, e.NewError(CodeFailedToGetTournamentResults, err))
		return
	}

	var players []ResultTournamentResponsePlayer
	for _, bill := range bills {
		if bill.TournamentID == nil || *bill.TournamentID != uint(tournamentID) {
			continue
		}
		if bill.Sum.Le(Zero) {
			continue
		}
		players = append(players, ResultTournamentResponsePlayer{
			PlayerID: bill.PlayerID,
			Prize:    bill.Sum,
		})
	}

	c.JSON(http.StatusOK, ResultTournamentResponse{
		StandardError: Success,
		Players:       players,
	})
}

// BalanceResponse represents a response for method Balance
type BalanceResponse struct {
	e.StandardError
	PlayerID uint    `json:"playerId"`
	Balance  Decimal `json:"balance"`
}

// Balance method to get player's balance
func Balance(c *gin.Context) {
	playerID, err := strconv.Atoi(c.Query("playerId"))
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, e.NewError(CodeWrongParams, err))
		return
	}

	s := service.Get().Storage
	balance, err := s.PlayerGetBalance(nil, uint(playerID))
	if err != nil {
		c.JSON(http.StatusInternalServerError, e.NewError(CodeFailedToGetBalance, err))
		return
	}

	c.JSON(http.StatusOK, BalanceResponse{
		StandardError: Success,
		PlayerID:      uint(playerID),
		Balance:       balance,
	})
}
